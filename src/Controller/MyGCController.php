<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MyGCController extends AbstractController
{
    /**
     * @Route("/my/g/c", name="my_g_c")
     */
    public function index()
    {
        return $this->render('my_gc/index.html.twig', [
            'controller_name' => 'MyGCController',
        ]);
    }
}
